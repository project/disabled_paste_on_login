(function ($, Drupal, window) {
  Drupal.behaviors.helpers = {
    attach: function (context, settings) {
      'use strict';

   if(settings.disabled_paste_on_login.disabled_paste_shortcut == 'disabled_paste_shortcut'){
    const pasteBox = document.getElementById("user-login-form");
      pasteBox.onpaste = e => {
      e.preventDefault();
      return false;
      };
   }

  if(settings.disabled_paste_on_login.disabled_copy == 'disabled_copy') {
    const copyBox = document.getElementById("user-login-form");
    copyBox.oncopy = e => {
    e.preventDefault();
    return false;
    };
    copyBox.oncut = e => {
    e.preventDefault();
    return false;
    };
 }

 if(settings.disabled_paste_on_login.disabled_right_click == 'disabled_right_click') {
    document.addEventListener('contextmenu', event => event.preventDefault());
 }

if(settings.disabled_paste_on_login.disabled_text_selection == 'disabled_text_selection') {

    const disableselect = (e) => {
    return false
    }
    document.onselectstart = disableselect
    document.onmousedown = disableselect

 }

}

};
})(jQuery, Drupal, window);

