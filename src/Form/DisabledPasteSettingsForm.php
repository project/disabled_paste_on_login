<?php

namespace Drupal\disabled_paste_on_login\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * disabled_paste_on_login Settings Form class.
 */
class DisabledPasteSettingsForm extends ConfigFormBase {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->getEditable('disabled_paste_on_login.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disabled_paste_on_login_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['disabled_paste_on_login.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['disabled_paste_on_login_config'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Body tag attributes & login form prevention'),
      '#description' => $this->t('Apply these attributes on login panel.'),
      '#options' => [
        'disabled_text_selection' => $this->t('Disable text selection'),
        'disabled_copy' => $this->t('Disable copy to clipboard'),
        'disabled_right_click' => $this->t('Disable right-click '),
        'disabled_paste_shortcut' => $this->t('Disabled paste on keyboard shortcut Ctr+v,Ctr+ins etc.'),
        'autocomplete_off' => $this->t('Disabled autocomplete=off on login form'),
      ],
      '#default_value' => !empty($this->config->get('disabled_paste_on_login_config')) ? $this->config->get('disabled_paste_on_login_config') : [],
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('disabled_paste_on_login.settings');
    $config->set('disabled_paste_on_login_config', $form_state->getValue('disabled_paste_on_login_config'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
