CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module includes several different technical ways/methods to make
copying/stealing/right click disbaled/prevent text selection on login panel of your site harder than it usually is:

Disable text selection
Disable copy to clipboard using keyboard and mouse
Autocomplete off
Disable paste
Disable right-click context menu on all site content

ACCESS URL:

URL: /admin/config/user-interface/disabled-paste-on-login


REQUIREMENTS
------------

This module does not have any dependency on any other module.

INSTALLATION
------------

Install as normal (see http://drupal.org/documentation/install/modules-themes).

CONFIGURATION
-------------

Once installed and enabled then configure the options at
"/admin/config/user-interface/disabled-paste-on-login" - all the methods are disabled by
default. The user permission (admin/people/permissions) "Bypass Disabled Paste
prevention" allows to not apply these module methods to trusted user roles.

MAINTAINERS
-----------
Pradip Mehta : https://www.drupal.org/u/pradip-mehta
